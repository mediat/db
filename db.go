package db

import (
	"log"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"gitlab.com/mediat/ingest"
)

var ddb *dynamodb.DynamoDB

func init() {
	ddb = dynamodb.New(ingest.AwsSession())
}

// FindByKey find by hashkey sortkey
func FindByKey(url, title string) (asset *ingest.Asset, err error) {
	out, err := ddb.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("asset"),
		Key: map[string]*dynamodb.AttributeValue{
			"url": {
				S: aws.String(url),
			},
			"title": {
				S: aws.String(title),
			},
		},
	})
	if err != nil {
		return
	}
	asset = &ingest.Asset{}
	err = dynamodbattribute.UnmarshalMap(out.Item, &asset)
	return
}

// Insert add new asset
func Insert(asset *ingest.Asset) error {
	av, err := dynamodbattribute.MarshalMap(asset)
	if err != nil {
		return err
	}
	_, err = ddb.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String("asset"),
		Item:      av,
	})
	return err
}

// Update update
func Update(asset *ingest.Asset) (map[string]*dynamodb.AttributeValue, error) {
	exprValues := make(map[string]*dynamodb.AttributeValue)
	updateExpr := make([]string, 0)
	exprAttrNames := make(map[string]*string)

	if asset.Thumbnail != "" {
		exprValues[":thumbnail"] = &dynamodb.AttributeValue{
			S: aws.String(asset.Thumbnail),
		}
		updateExpr = append(updateExpr, "set #thumbnail = :thumbnail")
		exprAttrNames["#thumbnail"] = aws.String("thumbnail")
	}
	if asset.Poster != "" {
		exprValues[":poster"] = &dynamodb.AttributeValue{
			S: aws.String(asset.Poster),
		}
		updateExpr = append(updateExpr, "set #poster = :poster")
		exprAttrNames["#poster"] = aws.String("poster")
	}
	if asset.Hash != "" {
		exprValues[":hash"] = &dynamodb.AttributeValue{
			S: aws.String(asset.Hash),
		}
		updateExpr = append(updateExpr, "set #hash = :hash")
		exprAttrNames["#hash"] = aws.String("hash")
	}
	if len(updateExpr) == 0 {
		log.Println("Nothing to update")
		return nil, nil
	}
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: exprValues,
		TableName:                 aws.String("asset"),
		Key: map[string]*dynamodb.AttributeValue{
			"url": {
				S: aws.String(asset.URL),
			},
			"title": {
				S: aws.String(asset.Title),
			},
		},
		ReturnValues:             aws.String("UPDATED_NEW"),
		UpdateExpression:         aws.String(strings.Join(updateExpr, ", ")),
		ExpressionAttributeNames: exprAttrNames,
	}
	out, err := ddb.UpdateItem(input)
	if err != nil {
		return nil, err
	}
	return out.Attributes, nil
}
