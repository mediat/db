FROM golang:1.9.2

WORKDIR /go/src/gitlab.com/mediat/db

ADD . .

RUN go-wrapper download
RUN go-wrapper install ./...

CMD ["go-wrapper", "run"]
