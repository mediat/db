package main

import (
	"encoding/json"
	"log"

	"github.com/satori/go.uuid"

	"gitlab.com/mediat/ingest"
	//"github.com/aws/aws-sdk-go/service/sqs"
	. "gitlab.com/mediat/db"
)

func main() {

	client := ingest.NewRedisClient()
	defer client.Close()
	pubsub := client.Subscribe(ingest.AssetAddCommand, ingest.AssetUpdateCommand)
	log.Println("Subscribe", ingest.AssetAddCommand, ingest.AssetUpdateCommand)
	var asset *ingest.Asset
	for msg := range pubsub.Channel() {
		log.Println("Got message on channel", msg.Channel)
		err := json.Unmarshal([]byte(msg.Payload), &asset)
		if err != nil {
			log.Println(err)
			continue
		}
		if asset.URL == "" {
			log.Println("Asset has no url, skip")
			continue
		}
		log.Println("FindByUrl", asset.URL)
		item, err := FindByKey(asset.URL, asset.Title)
		if err != nil {
			log.Println(err.Error())
			continue
		}

		if msg.Channel == ingest.AssetAddCommand && item.ID == nil {
			// Create new
			id := uuid.Must(uuid.NewV4())
			asset.ID = &id
			err = Insert(asset)
			if err != nil {
				log.Println(err)
				continue
			}
			log.Println("Created asset", asset.File, asset.URL)
			continue
		}

		if msg.Channel == ingest.AssetUpdateCommand {
			// Update
			out, err := Update(asset)
			if err != nil {
				log.Println("Update failed", err)
				continue
			}
			log.Println("Updated asset", out)
		}

	}
}
